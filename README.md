[![pipeline status](https://gitlab.com/bearcodi/what-the-tennis/badges/master/pipeline.svg)](https://gitlab.com/bearcodi/what-the-tennis/-/commits/master)
[![coverage report](https://gitlab.com/bearcodi/what-the-tennis/badges/master/coverage.svg)](https://gitlab.com/bearcodi/what-the-tennis/-/commits/master)


# Tennis API

Write a service to collect scores and report progress on a match of tennis. The problem is broken into stages.

## Scoring Games

Games are scored as follows:
 - games start at at "love" (or zero) and go up to 40, but that's actually just four points.
 - From love, the first point is 15, then 30, then 40, then game point.
 - If a player loses when on game point the score reverts to deuce (40 all)
 - The game continues until a player wins when on game point.

## Scoring Sets

 - To complete a set, someone must win six games; the first person to win six games wins the set
 - As with games - to win a set a player must win by two games.
 - If the score gets to 6 games all a tie breaker is played.

### Scoring Tie breaker
 - Players start at 0
 - Points are scored in single increments
 - First payer to win 7 points or more and be ahead by 2 points wins the set.


# The API

The API should be able to:
 - create a new game between two players
 - process points for a given game
 - report on the current score of a game.

The service does not need to persist data across restarts. Some indicative payloads are below.

## Create a game

```json
{
  "name": "Federer vs Nadal",
  "players": {
    "p1" : "Federer",
    "p2" : "Nadal"
  }
}
```

## Record a point

```json
{
  "gameId": 1234,
  "point": "p1"
}
```

## Request score

```json
{
    "gameId": 1234,
    "name": "Federer vs Nadal",
    "sets": [
        { "p1": 6, "p2": 4},
        { "p1": 1, "p2": 2}
    ],
    "currentGame": {
        "p1": 30,
        "p2": 40
    }
}
```

# Solution

I've changed the the `gameId` from the indicative payloads to `matchId`, additional the create _game_ has been changed to _match_ as a match contains games and I wanted to be avoid any confusion over the common termonology.

The [REST API documentation](./docs/openapi.yaml) can be accessed by navigating to the running applications root path, ie http://localhost:8080/. It has been documented using OpenAPI 3.0.0 schema.

## Running the service

System developed using:
- Node 14.16
- Express 4.17

Simple install and npm script gets you up and running.

```bash
npm install
npm run serve
```

By default the server runs on port `8080` this can be changed by creating a `.env` file in the project root from the `.env.example` file and changing the `APP_PORT` variable to the port of your choosing.
