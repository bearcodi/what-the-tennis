import supertest from 'supertest'
import app from '../../../src/app.js'
import Match from '../../../src/models/Match.js'
import { Point } from '../../../src/services/Game.js'
import matchFixtures from '../../fixtures/matches.json'

const API_BASE = '/api/v1/point';

describe('Point API Endpoint', () => {
    it('scores a point for a player in a match', async () => {
        const match = Match.create(matchFixtures[0]).start()
        const response = await supertest(app)
                                .post(`${API_BASE}/${match.id}`)
                                .send({
                                    player: 'p1'
                                })

        expect(response.statusCode).toEqual(202)
        
        expect(response.body.currentGame.p1).toBe(Point.Fifteen)
    })
})
