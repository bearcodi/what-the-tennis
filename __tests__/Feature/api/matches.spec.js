import supertest from 'supertest'
import app from '../../../src/app.js'
import Match from '../../../src/models/Match.js'
import { Point } from '../../../src/services/Game.js'
import matchFixtures from '../../fixtures/matches.json'

const API_BASE = '/api/v1/matches'

describe('Matches API Endpoint', () => {
    describe('Creates matches', () => {
        let response

        beforeEach(async () => {
            Match.truncate()
            response = await supertest(app).post(`${API_BASE}`).send(matchFixtures[0])
        })

        afterAll(() => {
            Match.truncate()
        })

        it('Creates a new match given two player names', () => {
            expect(response.statusCode).toBe(201)
            expect(response.body.players).toEqual(matchFixtures[0])
        })

        it('Has the match name attribute', () => {
            expect(response.body.name).toBe(Match.all()[0].name)
        })

        it('Has a match ID attribute', () => {
            expect(response.body.matchId).toBe(Match.all()[0].id)
        })

        it('Has a list of sets', () => {
            expect(response.body.sets).toBeInstanceOf(Array)
        })

        it('Has the score for the current game', () => {
            expect(response.body.currentGame).toEqual({
                p1: Point.Love,
                p2: Point.Love
            })
        });
    })

    describe('Lists matches', () => {
        beforeAll(async () => {
            Match.truncate()
            await Promise.all(matchFixtures.map((match) => supertest(app).post(`${API_BASE}`).send(match)))
        })

        it('can get a list of all matches', async () => {
            const response = await supertest(app).get(`${API_BASE}`)

            expect(response.statusCode).toEqual(200)
            expect(response.body.matches.length).toBe(2)
        })

        it('can find a match by id', async () => {
            const match = Match.all()[0]
            const response = await supertest(app).get(`${API_BASE}/${match.id}`)

            expect(response.statusCode).toEqual(200)
            expect(response.body.name).toEqual(match.name)
        })

        it('responds with a 404 when a match does not exist', async() => {
            const nonExistentMatchID = 0
            const response = await supertest(app).get(`${API_BASE}/${nonExistentMatchID}`)

            expect(response.statusCode).toEqual(404)
        })
    })
})
