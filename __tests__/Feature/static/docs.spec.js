import supertest from 'supertest'
import app from '../../../src/app.js'

describe('Documentation', () => {
    it('Displays the OpenAPI 3 REST schema', async (done) => {
        const response = await supertest(app).get('/')

        expect(response.statusCode).toBe(200)
        expect(response.text).toContain('openapi.yaml');
        
        done()
    })
})
