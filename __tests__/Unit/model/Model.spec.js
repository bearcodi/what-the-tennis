import store from '../../../src/store.js'
import Model from '../../../src/models/Model.js'

store.fakemodels = []
class FakeModel extends Model {}

describe('In memory Model data store', () => {
    beforeEach(() => {
        FakeModel.truncate();
    })

    it('Can truncate all stored models', () => {
        FakeModel.create()
        
        expect(FakeModel.all().length).toEqual(1)
        
        FakeModel.truncate()
        
        expect(FakeModel.all().length).toEqual(0)
    })

    it('Can store one or more models', () => {
        expect(FakeModel.all()).toEqual([])
        
        FakeModel.create();
        
        expect(FakeModel.all().length).toEqual(1)
    })

    it('Auto increments the `id` for a model when created', () => {
        expect(FakeModel.all()).toEqual([])
        
        const model = FakeModel.create()
        
        expect(FakeModel.all().length).toBe(1)
        expect(FakeModel.all()[0].id).toBe(model.id)
    })

    it('Can find a model by id', () => {
        const model = FakeModel.create()
        
        expect(FakeModel.find(model.id)).toEqual(model)
    })

    it('Returns false when a model cannot be found', () => {
        expect(FakeModel.find(1)).toBe(false)
    })

    it('Assigns properies to the model on creation', () => {
        const model = FakeModel.create({
            test: 'Hello'
        })

        expect(model.test).toEqual('Hello')
    })
});
