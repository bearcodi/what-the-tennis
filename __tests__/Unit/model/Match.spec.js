import Match from '../../../src/models/Match.js'
import { Point } from '../../../src/services/Game.js'
import matchFixtures from '../../fixtures/matches.json'

describe('Match data model', () => {
    beforeEach(() => {
        Match.truncate()
    })

    const players = matchFixtures[0]

    it('Creates a match name from the players names', () => {
        expect(Match.create(players).name).toBe(`${players.p1} vs ${players.p2}`)
    })

    it('Gets a players for a match', () => {
        expect(Match.create(players).players).toEqual(players)
    })

    it('Starts with the first set', () => {
        const match = Match.create(players).start()
        
        expect(match.sets.length).toBe(1)
    })

    it('Shows the score of the current game', () => {
        const match = Match.create(players).start()
        
        expect(match.currentGame.points).toEqual({
            p1: Point.Love,
            p2: Point.Love
        })
    })
    
    it('Scores a point for the current game in a set', () => {
        const match = Match.create(players).start()
        
        match.scorePoint('p1')
        
        expect(match.currentGame.points).toEqual({
            p1: Point.Fifteen,
            p2: Point.Love
        })
    })
    
    it('Starts a new set when a set is run', () => {
        const match = Match.create(players).start()
        const PointsToWinSetToLove = 24;
        Array(PointsToWinSetToLove).fill().forEach(() => match.scorePoint('p1'))
        expect(match.sets.length).toEqual(2)
    })

    it('Converts a match to JSON object', () => {
        const match = Match.create(players).start()

        expect(match.toJson()).toEqual({
            matchId: 1,
            name: `${players.p1} vs ${players.p2}`,
            players: {
                p1: players.p1,
                p2: players.p2
            },
            sets: [
                { p1: 0, p2: 0 }
            ],
            currentGame: {
                p1: Point.Love,
                p2: Point.Love
            }
        })
    })
});
