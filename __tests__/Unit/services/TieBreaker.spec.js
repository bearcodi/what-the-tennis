import TieBreaker from '../../../src/services/TieBreaker.js'

describe('Game', () => {
    let game;

    beforeEach(() => {
        game = new TieBreaker
    })

    it('score is 0 for a new game for both players', () => {
        expect(game._p1_point).toBe(0)
        expect(game._p2_point).toBe(0)
    })

    it('can score a point for a player', () => {
        game.scorePoint('p1')

        expect(game.points).toEqual({ p1: 1, p2: 0 })
    })

    it('awards the game to a player when they have won 7 or more points by a margin of 2', () => {
        game._p1_point = 6
        game._p2_point = 5

        expect(game.isWon()).toBeFalsy()
        
        game.scorePoint('p1')

        expect(game.isWon()).toBeTruthy()
        expect(game.points).toEqual({ p1: 7, p2: 5 })
    })

    it('determines the winning player', () => {

        game._p1_point = 6
        game._p2_point = 5

        game.scorePoint('p1')

        expect(game.winner).toEqual('p1')
    })
})
