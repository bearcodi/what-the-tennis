import { Game, Point } from '../../../src/services/Game.js'

describe('Game', () => {
    let game

    beforeEach(() => {
        game = new Game
    })

    describe('Scores for humans', () => {

        it('0 point is `Love`', () => {
            expect(Point.Love).toEqual('Love')
            expect(game.pointForHumans(0)).toEqual(Point.Love)
        })

        it('1 point is `15`', () => {
            expect(Point.Fifteen).toEqual('15')
            expect(game.pointForHumans(1)).toEqual(Point.Fifteen)
        })

        it('2 points is `30`', () => {
            expect(Point.Thirty).toEqual('30')
            expect(game.pointForHumans(2)).toEqual(Point.Thirty)
        })

        it('3 points is `40`', () => {
            expect(Point.Forty).toEqual('40')
            expect(game.pointForHumans(3)).toEqual(Point.Forty)
        })

        it('4 points is `Game`', () => {
            const wonGame = new Game
            wonGame._p1_point = 4

            expect(wonGame.pointForHumans(4)).toEqual('')
        })

        it('both players have 4 points is `Deuce`', () => {
            const game = new Game
            game._p1_point = 4
            game._p2_point = 4
            
            expect(Point.Deuce).toEqual('Deuce')
            expect(game.pointForHumans(4)).toEqual(Point.Deuce)
        })

        it('on player has more than 4 points but not winning by 2 `Advantage`', () => {
            const game = new Game
            game._p1_point = 5
            game._p2_point = 4
            
            expect(Point.Advantage).toEqual('Advantage')
            expect(game.pointForHumans(4)).toEqual(Point.Advantage)
        })

        it('shows the scores for humans', () => {
            expect(game.points).toEqual({
                p1: Point.Love,
                p2: Point.Love
            })
        })
    })

    describe('Scoring', () => {

        it('score is 0 for a new game for both players', () => {
            expect(game._p1_point).toBe(0)
            expect(game._p2_point).toBe(0)
        })

        it('can score a point for a player', () => {
            expect(game.points).toEqual({ p1: Point.Love, p2: Point.Love })

            game.scorePoint('p1')

            expect(game.points).toEqual({ p1: Point.Fifteen, p2: Point.Love })
        })

        it('awards a game to a player when they have won more than 4 points', () => {
            expect(game.isWon()).toBeFalsy()

            Array(4).fill().forEach(() => game.scorePoint('p1'))
            
            expect(game.isWon()).toBeTruthy();
        })

        it('determines the winning player', () => {
            expect(game.winner).toBeNull()
            
            Array(4).fill().forEach(() => game.scorePoint('p2'))

            expect(game.isWon()).toBeTruthy()
            expect(game.winner).toEqual('p2')
        })
    })
})
