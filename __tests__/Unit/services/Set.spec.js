import Set from '../../../src/services/Set.js'
import {Game, Point} from '../../../src/services/Game.js'
import TieBreaker from '../../../src/services/TieBreaker.js'

describe('Set', () => {
    let set;

    describe('Game scoring', () => {
        beforeEach(() => {
            set = new Set;
        })

        it('starts at 0 games for both players', () => {
            expect(set._p1_games).toBe(0)
            expect(set._p2_games).toBe(0)
        })

        it('has a current game', () => {
            expect(set.currentGame).toBeInstanceOf(Game)
        })

        it('can score a point for the current game', () => {
            set.scorePoint('p1')
            
            expect(set.currentGame.p1_point).toBe(Point.Fifteen);
        })

    })

    describe('Set scoring', () => {
        beforeEach(() => {
            set = new Set;
        })

        it('increments the sets game number when a game is won', () => {    
            Array(4).fill().forEach(() => set.scorePoint('p1'))    
            Array(4).fill().forEach(() => set.scorePoint('p2'))

            expect(set.p1_games).toEqual(1)
            expect(set.p2_games).toEqual(1)
        })

        it('awards a set to a player who wins 6 games and more than 2 games ahead', () => {
            set._p1_games = 5
            set._p2_games = 3

            expect(set.isWon()).toBeFalsy()
            
            Array(4).fill().forEach(() => set.scorePoint('p1'))

            expect(set.isWon()).toBeTruthy()
            expect(set.score).toEqual({
                p1: 6,
                p2: 3
            })
            expect(set.currentGame).toBeNull()
        })


        it('awards a set to a player who wins 7 games and is 2 games ahead', () => {
            set._p1_games = 6
            set._p2_games = 5

            expect(set.isWon()).toBeFalsy()
            
            Array(4).fill().forEach(() => set.scorePoint('p1'))

            expect(set.isWon()).toBeTruthy()
            expect(set.score).toEqual({
                p1: 7,
                p2: 5
            })
            expect(set.currentGame).toBeNull()
        })

        it('goes into a tiebreaker when both players win 6 games', () => {
            set._p1_games = 5
            set._p2_games = 6
            
            Array(4).fill().forEach(() => set.scorePoint('p1'))

            expect(set.isWon()).toBeFalsy()
            expect(set.score).toEqual({
                p1: 6,
                p2: 6
            })
            expect(set.currentGame).toBeInstanceOf(TieBreaker)
        })

        it('awards the set to the winner of a tiebreaker', () => {
            set._p1_games = 5
            set._p2_games = 6
            Array(4).fill().forEach(() => set.scorePoint('p1'))

            expect(set.currentGame).toBeInstanceOf(TieBreaker)
            
            Array(7).fill().forEach(() => set.scorePoint('p1'))

            expect(set.isWon()).toBeTruthy()
            expect(set.currentGame).toBeNull()
            expect(set.score).toEqual({
                p1: 7,
                p2: 6
            })
        })
    })
})
