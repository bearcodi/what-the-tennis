import app from './src/app.js'
import dotenv from 'dotenv'

dotenv.config()

const port = process.env.APP_PORT || 8080

app.listen(port, () => {
    console.log(`Tenvis is running at http://localhost:${port}`)
});
