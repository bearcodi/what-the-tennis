import store from '../store.js'
import pluralize from 'pluralize'

export default class Model {
    /**
     * Creates an unsigned ID.
     *
     * @returns {void}
     */
    autoIncrement() {
        this.id = this.getStore().length + 1;
    }

    /**
     * Determine the storeable table name from Model implementation.
     *
     * @returns {string}
     */
    getStoreableName() {
        return pluralize(this.constructor.name.toLowerCase())
    }

    /**
     * Get store collection for Model implementation.
     *
     * @returns {array}
     */
    getStore() {
        return store[this.getStoreableName()]
    }

    /**
     * Persists the model to the store.
     *
     * @returns {self}
     */
    save() {
        this.autoIncrement()
        this.getStore().push(this)
        return this
    }

    /**
     * Create a new match.
     *
     * @param {object} properties
     *
     * @returns {self}
     */
    static create(properties = {}) {
        const model = new this;

        Object.entries(properties).forEach(property => model[property[0]] = property[1])

        return model.save()
    }

    /**
     * Find model by `id`.
     *
     * @param {int|string} id
     * @returns {this|false} when model can't be found.
     */
    static find(id) {
        return (new this).getStore().find(model => model.id === parseInt(id)) || false
    }

    /**
     * Retreive all models.
     *
     * @returns {array}
     */
    static all() {
        return (new this).getStore()
    }

    /**
     * Truncate all model records.
     *
     * @returns {this}
     */
    static truncate() {
        store[(new this).getStoreableName()] = []
        return this
    }
}
