import Model from './Model.js'
import Set from '../services/Set.js'

export default class Match extends Model {
    /**
     * Match name attribute accessor.
     *
     * @returns {string}
     */
    get name() {
        return `${this.p1} vs ${this.p2}`
    }

    /**
     * Get the match players.
     *
     * @returns {object}
     */
    get players() {
        return {
            p1: this.p1,
            p2: this.p2
        }
    }

    /**
     * Start the match.
     *
     * @returns {Match}
     */
    start() {
        this.sets = []
        this.startNewSet()
        return this
    }
    
    /**
     * Start a new set.
     * 
     * @returns {Match}
     */
    startNewSet() {
        this.sets.push(new Set)
        
        return this
    }

    /**
     * Get the current game.
     *
     * @returns {Game}
     */
    get currentGame() {
        return this.currentSet.currentGame
    }
    
    /**
     * Get the current set.
     * 
     * @returns {Set}
     */
    get currentSet() {
        return this.sets[this.sets.length - 1]
    }
    
    /**
     * Score a point for a player.
     * 
     * @param  {string} player Either `p1` or `p2`
     * @return {Match}
     */
    scorePoint(player) {
        this.currentSet.scorePoint(player)
        if (this.currentSet.isWon()) {
            this.startNewSet()
        }
        return this
    }

    /**
     * JSON representation of the match model.
     *
     * @returns {object}
     */
    toJson() {
        return {
            matchId: this.id,
            name: this.name,
            players: this.players,
            sets: this.sets.map(set => set.score),
            currentGame: this.currentGame.points
        }
    }
}
