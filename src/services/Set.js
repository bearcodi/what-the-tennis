import Game from './Game.js'
import TieBreaker from './TieBreaker.js'

export default class Set {
    /**
     * Start a set with a new game.
     *
     * @returns {void}
     */
    constructor() {
        this._p1_games = 0
        this._p2_games = 0
        this._tieBreaker = false
        this._currentGame = null

        this.newGame()
    }

    /**
     * Get player one's game total.
     *
     * @var {integer}
     */
    get p1_games() {
        return this._p1_games
    }

    /**
     * Get player twos's game total.
     *
     * @var {integer}
     */
    get p2_games() {
        return this._p2_games
    }

    /**
     * Get the sets score.
     *
     * @return {object}
     */
    get score() {
        return {
            p1: this._p1_games,
            p2: this._p2_games
        }
    }

    /**
     * Get the current game.
     *
     * @return {Game|null}
     */
    get currentGame() {
        return this._currentGame
    }

    /**
     * Start a new game.
     *
     * @returns {void}
     */
    newGame() {
        if (this.isTieBreakerRequired()) {
            this._currentGame = new TieBreaker
            this._tieBreaker = true
        } else {
            this._currentGame = new Game
        }
    }

    /**
     * Score a point in the current game.
     *
     * @param {string} player
     * @returns {Set}
     */
    scorePoint(player) {
        this._currentGame.scorePoint(player)
        this.awardGame()
        return this
    }

    /**
     * Does the set need to head to a tie breaker.
     *
     * @returns {boolean}
     */
    isTieBreakerRequired() {
        return this._p1_games === 6 && this._p2_games === 6
    }

    /**
     * Award the game to a player.
     *
     * @returns {boolean}
     */
    awardGame() {
        if (!this._currentGame.isWon()) {
            return false
        }

        if (this._currentGame.winner === 'p1') {
            this._p1_games += 1
        }

        if (this.currentGame.winner === 'p2') {
            this._p2_games += 1
        }

        this.newGame()

        return true
    }

    /**
     * Has the set been won by a player.
     *
     * @returns {boolean}
     */
    isWon() {
        if ((this._p1_games > 5 || this._p2_games > 5)
            && !this.isTieBreakerRequired()
            && Math.abs(this._p1_games - this._p2_games) >= 2) {
            this._currentGame = null
            return true
        }

        if (this._tieBreaker && Math.abs(this._p1_games - this._p2_games) === 1) {
            this._currentGame = null
            return true
        }

        return false
    }
}
