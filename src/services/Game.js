/**
 * Game point human readable enum,
 * 
 * @var {object}
 */
export const Point = {
    Love: 'Love',
    Fifteen: '15',
    Thirty: '30',
    Forty: '40',
    Advantage: 'Advantage',
    Deuce: 'Deuce'
}

export class Game {
    
    /**
     * Initialise default properies
     *
     * @returns {void}
     */
    constructor() {
        this.WIN_BY = 2
        this.MIN_WINNING_POINTS = 4
        this._p1_point = 0
        this._p2_point = 0
    }

    /**
     * Get both player's games points.
     *
     * @returns {object}
     */
    get points() {
        return {
            p1: this.p1_point,
            p2: this.p2_point
        }
    }

    /**
     * Get the translated player one's game points.
     *
     * @returns {string}
     */
    get p1_point() {
        return this.pointForHumans(this._p1_point)
    }

    /**
     * Get the translated player's two game points.
     *
     * @returns {string}
     */
    get p2_point() {
        return this.pointForHumans(this._p2_point)
    }

    /**
     * Get the game winner.
     *
     * @returns {string|null} when the game has not been won.
     */
    get winner() {
        return this.isWon()
            ? (this._p1_point > this._p2_point ? 'p1' : 'p2')
            : null
    }

    /**
     * Score a point for a player.
     *
     * @param {string} player
     * @returns {Game}
     */
    scorePoint(player) {
        if (player === 'p1') {
            this._p1_point += 1
        }

        if (player === 'p2') {
            this._p2_point += 1
        }

        return this
    }

    /**
     * Get the human readable version of the point.
     *
     * @param {int} point
     * @returns {string}
     */
    pointForHumans(point) {
        if (this.isWon()) {
            return ''
        }
        
        let humanPoint;

        switch (point) {
            case 0:
                humanPoint = Point.Love
                break;
            case 1:
                humanPoint = Point.Fifteen
                break;
            case 2:
                humanPoint = Point.Thirty
                break;
            case 3:
                humanPoint = Point.Forty
                break;
            default:
                humanPoint = this.isAdvantage() ? Point.Advantage : Point.Deuce
        }

        return humanPoint
    }

    /**
     * Is game state in deuce.
     *
     * @returns {boolean}
     */
    isDeuce() {
        return this._p1_point >= this.MIN_WINNING_POINTS
            && this._p2_point >= this.MIN_WINNING_POINTS
            && this._p1_point === this._p2_point;
    }

    /**
     * Is the game state in advantage.
     *
     * @returns {boolean}
     */
    isAdvantage() {
        return !this.isDeuce()
            && Math.abs(this._p1_point - this._p2_point) < this.WIN_BY;
    }

    /**
     * Is the game won.
     *
     * @returns {boolean}
     */
    isWon() {
        if (this._p1_point < 4 && this._p2_point < 4) {
            return false
        }

        if (this.isDeuce() || this.isAdvantage()) {
            return false
        }

        return true;
    }
}

export default Game
