import Game from './Game.js'

export default class TieBreaker extends Game {
    /**
     * Override parent properties.
     *
     * @returns {void}
     */
    constructor() {
        super()
        this.MIN_WINNING_POINTS = 7
    }
    
    /**
     * Player one's point.
     *
     * @returns {integer}
     */
    get p1_point() {
        return this._p1_point
    }

    /**
     * Player two's current point.
     *
     * @returns {integer}
     */
    get p2_point() {
        return this._p2_point
    }

    /**
     * Is the game won.
     *
     * @returns {boolean}
     */
    isWon() {
        return (this._p1_point >= this.MIN_WINNING_POINTS || this._p2_point >= this.MIN_WINNING_POINTS)
            && Math.abs(this._p1_point - this._p2_point) >= this.WIN_BY
    }
}
