import express from 'express'
import staticRoutes from './routes/static.js'
import apiRoutes from './routes/api.js'

const app = express()

app.use(express.json())
   .use(express.urlencoded({
       extended: true
    }));

/**
 * Register static paths.
 */
staticRoutes.forEach((staticRoute) => {
    app.use(staticRoute)
})

/**
 * Register API routes.
 */
app.use('/api/v1', apiRoutes)

export default app
