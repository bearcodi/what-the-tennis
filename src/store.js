/**
 * Super simple temporary memory store.
 *
 * This thing only lives as long as your server runs.
 */
export default {
    matches: [],
}
