import express from 'express'
import MatchController from '../controllers/MatchController.js'
import PointController from '../controllers/PointController.js'

const api = express.Router()

api.get('/matches/:match_id', MatchController.show)
api.get('/matches', MatchController.index)
api.post('/matches', MatchController.store)

api.post('/point/:match_id', PointController.store)

export default api
