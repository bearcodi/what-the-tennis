import path from 'path'
import express from 'express'

export default [
    'public',
    'docs'
].map((staticPath) => {
    return express.static(path.resolve(staticPath))
})
