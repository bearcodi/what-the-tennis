import Match from '../models/Match.js'

export default class MatchController {
    /**
     * List all matches.
     *
     * @params {request}
     * @params {response}
     * @returns {response.json}
     */
    static index(request, response) {
        const matches = Match.all().map((match) => match.toJson());

        return response.json({ matches })
    }

    /**
     * Get a match by ID.
     *
     * @params {request}
     * @params {response}
     * @returns {response.json}
     */
    static show(request, response) {
        const match = Match.find(parseInt(request.params.match_id))

        if (!match) {
            return response.status(404).json({ message: 'Match not found!' })
        }

        return response.json(match.toJson())
    }
    
    /**
     * Creates a new match.
     *
     * @params {request}
     * @params {response}
     * @returns {response.json}
     */
    static store(request, response) {
        const match = Match.create(request.body).start()
        
        return response.status(201).json(match.toJson())
    }
}
