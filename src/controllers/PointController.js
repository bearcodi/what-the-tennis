import Match from '../models/Match.js'

export default class PointController
{
    /**
     * Creates a new match.
     *
     * @params {request}
     * @params {response}
     * @returns {response.json}
     */
    static store(request, response) {
        const match = Match.find(request.params.match_id)

        match.scorePoint(request.body.player)

        return response.status(202).json(match.toJson())
    }
}
